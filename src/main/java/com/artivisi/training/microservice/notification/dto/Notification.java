package com.artivisi.training.microservice.notification.dto;

import lombok.Data;

@Data
public class Notification {
    private String from;
    private String to;
    private String subject;
    private String body;
}
