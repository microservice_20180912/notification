package com.artivisi.training.microservice.notification;

import com.artivisi.training.microservice.notification.dto.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class NotificationApplication  {

    public static Logger logger = LoggerFactory.getLogger(NotificationApplication.class);

    @Autowired private ObjectMapper objectMapper;
    
    public static void main(String[] args) {
        SpringApplication.run(NotificationApplication.class, args);
    }
    
    @KafkaListener(topics = "coba")
    public void handleMessage(String message) throws Exception {
        logger.info(message);
        Notification notif = objectMapper.readValue(message, Notification.class);
        System.out.println("From : " +notif.getFrom());
        System.out.println("To : " +notif.getTo());
        System.out.println("Subject : " +notif.getSubject());
        System.out.println("Body : " +notif.getBody());
        
    }
    
    //@KafkaListener(topics = "coba")
    public void handleMessageAsObject(Notification notif) throws Exception {
        System.out.println("From : " +notif.getFrom());
        System.out.println("To : " +notif.getTo());
        System.out.println("Subject : " +notif.getSubject());
        System.out.println("Body : " +notif.getBody());
        
    }

}
